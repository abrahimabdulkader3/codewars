function xor(a, b) {

    // If only one out of the 2 expressions are true, return true

    if (a == false && b == false) {
        return false;
    }
    else if(a == false && b == true) {
        return true;
    }

    else if(a == true && b == true) {
        return false;
    }

    else if(a == true && b == false) {
        return true
    }
}

console.log(xor(false, false))
console.log(xor(false, true))
console.log(xor(true, true))
console.log(xor(true, false))
