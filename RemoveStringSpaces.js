function noSpace(x){

    //Create an empty string
    let result = "";

    //Loop through the string
    for(let i = 0; i < x.length; i++) {
      if(x[i] != ' ') { //If the character at index i is not a space
        result += x[i] //Take that character and append it to the result
      }
    }
    console.log(result)
    return result;
  }

console.log(noSpace('8 j 8   mBliB8g  imjB8B8  jl  B'))
